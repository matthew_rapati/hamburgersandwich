# README #

This is the code for the website behind hamburgersandwich.com. It is made available here for educational purposes!

### How do I get set up? ###

* install node and npm
* type "npm install" to install npm dependencies
* type "gulp" to run tasks
* type "node server.js" to start the server
* go to http://localhost:8080