function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};

var CanvasBg = function(canvas, lines) {
	this.canvas = canvas;
	this.canvas2d = canvas.getContext('2d');
	this.lines = lines;
	this.rects = [];
};

CanvasBg.prototype = function() {
	var init = function() {
		for (var i = 0; i < this.lines; i++) {
			this.rects.push(
				new rect(
					this.canvas2d,
					0,
					Math.floor(this.canvas.height / this.lines * i),
					this.canvas.width,
					Math.ceil(this.canvas.height / this.lines),
					getRandomColour()
				)
			);
		}

		bindEvents.call(this);
	},
	bindEvents = function() {
		window.addEventListener('mousemove', function(e) {
			changeRectColorAt.call(this, parseInt(e.clientX), parseInt(e.clientY));
		}.bind(this));

		window.addEventListener('resize', debounce(function() {
			this.canvas.width = document.body.clientWidth;
			this.canvas.height = document.body.clientHeight;
			update.call(this);
		}, 250).bind(this));
	},
	update = function() {
		for (var i = 0; i < this.rects.length; i++) {
			this.rects[i].height = Math.ceil(this.canvas.height / this.rects.length);
			this.rects[i].y = Math.floor(this.canvas.height / this.rects.length * i);
			this.rects[i].width = this.canvas.width;
			this.rects[i].draw();
		}
	},
	changeRectColorAt = function(x, y) {
		for (var i = 0; i < this.rects.length; i++) {
			if (this.rects[i].isPointInside(x, y)) {
				if (this.rects[i].colorChanged === false) {
					this.rects[i].fill = getRandomColour();
				}
				
				this.rects[i].colorChanged = true;
			} else {
				this.rects[i].colorChanged = false;
			}

			this.rects[i].draw();
		}
	},
	getRandomColour = function() {
		var hue = getRandomInt(0, 360);
		var sat = getRandomInt(95, 100);
		var lig = getRandomInt(50, 70);

		return 'hsla(' + hue + ',' + sat + '%,' + lig + '%,1' + ')';
	},
	getRandomInt = function(min, max) {
		return Math.floor(Math.random() * (max - min)) + min;
	},
	rect = (function() {
		function rect(canvas2d, x, y, width, height, fill) {
			this.canvas2d = canvas2d;
			this.x = x;
			this.y = y;
			this.width = width;
			this.height = height;
			this.fill = fill || "gray";
			this.draw(this.x, this.y);
			this.colorChanged = false;
			
			return (this);
		}

		rect.prototype.draw = function() {
			this.canvas2d.save();
			this.canvas2d.beginPath();
			this.canvas2d.fillStyle = this.fill;
			this.canvas2d.rect(this.x, this.y, this.width, this.height);
			this.canvas2d.fill();
			this.canvas2d.restore();
		};

		rect.prototype.isPointInside = function(x, y) {
			return (x >= this.x && x <= this.x + this.width && y >= this.y && y <= this.y + this.height);
		};

		return rect;
	})();

	return {
		init: init
	};
}();

var canvas = document.getElementById('background');
canvas.width = document.body.clientWidth,
canvas.height = document.body.clientHeight;

var canvasBg = new CanvasBg(canvas, 12);
canvasBg.init();